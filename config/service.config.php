<?php

return array(
	'factories' => array(
        'ZF\OAuth2\Service\OAuth2Server'                 => 'ZF\MvcAuth\Factory\NamedOAuth2ServerFactory',
        'Api\\Security\\Authentication\\IdentityProvider'   => 'Api\\Security\\Authentication\\Factories\\IdentityProviderFactory',
        'Api\\Security\\Authentication\\Authorization'      => 'Api\\Security\\Authentication\\Factories\\AuthorizationFactory',
     ),

    'invokables' => array(
        'Zend\Authentication\AuthenticationService' => 'Zend\Authentication\AuthenticationService',
    ),

    'delegators' => array(
        'ZF\OAuth2\Service\OAuth2Server' => array(
             'Api\Security\Factories\OAuth2InstanceDelegatorFactory'
        ),
        
    ),

    'aliases' => array(
        'ZF\MvcAuth\Authorization\AuthorizationInterface' => 'Api\\Security\\Authentication\\Authorization',
    ),
);