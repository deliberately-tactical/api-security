<?php

namespace Api\Security\Factories;

use Api\Security\GrantTypes\UserTokenGrant;
use Zend\ServiceManager\DelegatorFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class OAuth2InstanceDelegatorFactory implements DelegatorFactoryInterface
{
    public function createDelegatorWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName, $callback)
    {
        $oauth = call_user_func($callback(), '/login');
        $oauth->addGrantType(new UserTokenGrant($oauth->getStorage('user_credentials'), $serviceLocator));
        
        return function () use ($oauth) {
                return $oauth;
        };
        
    }
}