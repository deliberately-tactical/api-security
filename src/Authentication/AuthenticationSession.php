<?php

namespace Api\Security\Authentication;


use Zend\Session\Container as SessionContainer;
use Zend\Authentication\Storage\StorageInterface;
use Zend\Session\ManagerInterface as SessionManager;
use ZF\MvcAuth\Identity\AuthenticatedIdentity;

class AuthenticationSession implements StorageInterface {

    const NAMESPACE_DEFAULT = 'Grace_Session';

    const MEMBER_DEFAULT = 'storage';

    protected $session;

    protected $namespace = self::NAMESPACE_DEFAULT;

    protected $member = self::MEMBER_DEFAULT;

    public function __construct($namespace = null, SessionManager $manager = null) {
        if ($namespace !== null) {
            $this->namespace = $namespace;
        }
        
        $this->session   = new SessionContainer($this->namespace, $manager);
    }

    public function isEmpty() {
        return !isset($this->session->{$this->member});
    }

    public function read() {
        return $this->session->{$this->member};
    }

    public function write($contents) {
        
        if($contents instanceof AuthenticatedIdentity) {
            $identity = $contents->getAuthenticationIdentity();   
        
            if(is_array($identity) && !empty($identity)) {
                $this->session->{$this->member} = $identity;

                return $this;    
            }
        }

        return false;
    }

    public function clear() {
        unset($this->session->{$this->member});
    }
}