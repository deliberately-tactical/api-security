<?php

namespace Api\Security\Authentication;

use ZF\MvcAuth\Authorization\AuthorizationInterface;
use ZF\MvcAuth\Identity\IdentityInterface;
use ZfcRbac\Service\AuthorizationService;

class Authorization implements AuthorizationInterface
{
    /** @var  AuthorizationService */
    private $authorizationService;
    private $config = [];

    public function setAuthorizationService(AuthorizationService $authorizationService)
    {
        $this->authorizationService = $authorizationService;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * Whether or not the given identity has the given privilege on the given resource.
     *
     * @param IdentityInterface $identity
     * @param mixed $resource
     * @param mixed $privilege
     * @return bool
     */
    public function isAuthorized(IdentityInterface $identity, $resource, $privilege)
    {
        
        $restGuard = $this->config['rest_guard'];
        list($controller, $group) = explode('::', $resource);
        

        if (isset($restGuard[$controller][$group][$privilege])) {
            $result = $restGuard[$controller][$group][$privilege];
            if (is_array($result)) {
                $and = true;
                foreach ($result as $r) {
                    $and = $and && $this->authorizationService->isGranted($r);
                }
                $result = $and;
            }
            return $result;
        }
        
        return true;
    }

}