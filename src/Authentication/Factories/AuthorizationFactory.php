<?php

namespace Api\Security\Authentication\Factories;

use \Zend\ServiceManager\ServiceManager;
use Api\Security\Authentication\Authorization;

class AuthorizationFactory
{
    public function __invoke(ServiceManager $services)
    {
        /** @var \ZfcRbac\Service\AuthorizationService $authorizationService */
        $authorizationService = $services->get('ZfcRbac\Service\AuthorizationService');

        $config = $services->get('config');
        $rbacConfig = $config['zfc_rbac'];
        $authorization = new Authorization();
        $authorization->setConfig($rbacConfig);
        $authorization->setAuthorizationService($authorizationService);
        return $authorization;
    }
}